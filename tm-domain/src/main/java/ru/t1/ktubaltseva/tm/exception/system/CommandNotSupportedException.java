package ru.t1.ktubaltseva.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(@NotNull final String command) {
        super("Error! Command \"" + command + "\" not supported...");
    }

}
