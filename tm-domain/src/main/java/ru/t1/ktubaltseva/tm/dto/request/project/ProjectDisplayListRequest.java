package ru.t1.ktubaltseva.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;
import ru.t1.ktubaltseva.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDisplayListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public ProjectDisplayListRequest(@Nullable final String token) {
        super(token);
    }

    public ProjectDisplayListRequest(@Nullable final String token, @Nullable Sort sort) {
        super(token);
        this.sort = sort;
    }

}
