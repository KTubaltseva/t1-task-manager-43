package ru.t1.ktubaltseva.tm.dto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class ProjectDTO extends AbstractUserOwnedModelWBSDTO {

    private static final long serialVersionUID = 1;

    public ProjectDTO(@NotNull final UserDTO user, @NotNull final String name) {
        super(user, name);
    }

}
