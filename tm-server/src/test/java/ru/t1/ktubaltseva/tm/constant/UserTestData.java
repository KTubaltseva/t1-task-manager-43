package ru.t1.ktubaltseva.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.enumerated.Role;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class UserTestData {

    @Nullable
    public final static String USER_1_LOGIN = "USER_1_LOGIN";

    @Nullable
    public final static String USER_1_PASSWORD = "USER_1_PASSWORD";

    @Nullable
    public final static String USER_1_EMAIL = "USER_1_EMAIL";

    @Nullable
    public final static String USER_2_LOGIN = "USER_2_LOGIN";

    @Nullable
    public final static String USER_2_PASSWORD = "USER_2_PASSWORD";

    @Nullable
    public final static String USER_2_EMAIL = "USER_2_EMAIL";

    @NotNull
    public final static UserDTO USER_1 = new UserDTO(USER_1_LOGIN, USER_1_PASSWORD, USER_1_EMAIL);

    @NotNull
    public final static UserDTO USER_2 = new UserDTO(USER_2_LOGIN, USER_2_PASSWORD, USER_2_EMAIL);
    @Nullable
    public final static UserDTO NULL_PROJECT = null;

    @Nullable
    public final static UserDTO NULL_USER = null;

    @Nullable
    public final static String NULL_USER_ID = null;

    @Nullable
    public final static String NON_EXISTENT_USER_ID = "NON_EXISTENT_USER_ID";

    @Nullable
    public final static UserDTO NON_EXISTENT_USER = new UserDTO();

    @Nullable
    public final static String USER_EMAIL = "TestEmail";

    @Nullable
    public final static String NON_EXISTENT_USER_EMAIL = "NON_EXISTENT_USER_EMAIL";

    @Nullable
    public final static String NULL_EMAIL = null;

    @Nullable
    public final static String USER_PASSWORD = "TestPassword";

    @Nullable
    public final static String NULL_PASSWORD = null;

    @Nullable
    public final static String USER_FIRST_NAME = "USER_FIRST_NAME";

    @Nullable
    public final static String NULL_FIRST_NAME = null;

    @Nullable
    public final static String USER_MIDDLE_NAME = "USER_MIDDLE_NAME";

    @Nullable
    public final static String NULL_MIDDLE_NAME = null;

    @Nullable
    public final static String USER_LAST_NAME = "USER_LAST_NAME";

    @Nullable
    public final static String NULL_LAST_NAME = null;

    @Nullable
    public final static Role USER_ROLE = Role.USUAL;

    @Nullable
    public final static Role NULL_ROLE = null;

    @Nullable
    public final static String USER_LOGIN = "TestLogin";

    @Nullable
    public final static String NON_EXISTENT_USER_LOGIN = "NON_EXISTENT_USER_LOGIN";

    @Nullable
    public final static String NULL_LOGIN = null;

    @NotNull
    public final static UserDTO USER_WITH_LOGIN_EMAIL = new UserDTO(USER_LOGIN, USER_PASSWORD, USER_EMAIL);

    @NotNull
    public final static List<UserDTO> USER_LIST = Arrays.asList(USER_1, USER_2, USER_WITH_LOGIN_EMAIL);

}
