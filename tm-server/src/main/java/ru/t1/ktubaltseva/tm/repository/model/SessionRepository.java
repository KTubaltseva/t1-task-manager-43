package ru.t1.ktubaltseva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.repository.model.ISessionRepository;
import ru.t1.ktubaltseva.tm.comparator.CreatedComparator;
import ru.t1.ktubaltseva.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.Comparator;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<Session> getClazz() {
        return Session.class;
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        return "created";
    }

}
