package ru.t1.ktubaltseva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.dto.IDTORepository;
import ru.t1.ktubaltseva.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractDTORepository<M extends AbstractModelDTO> implements IDTORepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    @NotNull
    protected abstract Class<M> getClazz();

    @NotNull
    protected abstract String getSortColumnName(@NotNull final Comparator comparator);

    public AbstractDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM " + getClazz().getSimpleName();
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName();
        return entityManager.createQuery(jpql, getClazz()).getResultList();
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName() + " ORDER BY " + getSortColumnName(comparator);
        return entityManager.createQuery(jpql, getClazz()).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return entityManager.find(getClazz(), id);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(*) FROM " + getClazz().getSimpleName();
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void removeById(@NotNull final String id) {
        Optional<M> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Nullable
    @Override
    public M update(@NotNull final M model) {
        return entityManager.merge(model);
    }

}
