package ru.t1.ktubaltseva.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.model.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.model.ITaskService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskService extends AbstractUserOwnedWBSService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable List<Task> resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository modelRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultModel = modelRepository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

}
